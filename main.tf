resource "local_file" "foo" {
  filename             = "${path.module}/foo.bar"
  content              = "bar!"
  directory_permission = "0755"
  file_permission      = "0755"
}

// module "random_pet" {
//   source  = "gitlab.com/gitlab-org/random-pet/local"
//   version = "0.0.1"
// }
